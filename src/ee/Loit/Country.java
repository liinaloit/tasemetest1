package ee.Loit;

import java.util.ArrayList;
import java.util.List;

public class Country {
    String populationOfCountry;
    String nameofCountry;
    String languages;

    public static List<Country> listOfPopulation = new ArrayList<>();

    public Country(String population, String languages, String name) {
        this.populationOfCountry = population;
        this.nameofCountry = name;
        this.languages = languages;
        listOfPopulation.add(this);
    }

    public String getPopulation() {
        return populationOfCountry;
    }

    public void setPopulation(String population) { }

    public String getName() {
        return nameofCountry;
    }

    public void setName(String name) { }

    public String toString(){
        return String.format("%s riigis elavad %s, kes räägivad %s keelt", nameofCountry, populationOfCountry, languages);
    }
}